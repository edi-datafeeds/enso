
WFI - Series WFI ENSO

KEY
* = primary key


----------					
Series ENSO WFI

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
Actflag	Char	Char String	1	ACTION	Event Level Action Status
Created	Date	yyyy/mm/dd	10		Date event first entered
Changed	Date	yyyy/mm/dd hh:mm:ss	19		Date event updated.
*SecId	Integer	32 bit	10		Security linking key
IssID	Integer	32 bit	10		Link to ISSUR TABLE
*BbeID	Integer	32 bit	10		Primary key this table
*ScexhID	Integer	32 bit	10		Unique global level Listing ID
*SedolId	Integer	32 bit	10		Unique Internal Counter for this table
SedolActflag	Char	Char String	1	ACTION	Event Level Action Status
ISIN	Char	Char String	12		ISIN from SCMST for reference purposes
USCode	Char	Char String	9		
Sedol	Char	Char String	7		
SedolDefunct	Char	Char String	1	BOOLEAN	London Stock Exchange indicates code is defunct
SedolRegCntryCD	Char	Char String	2	CNTRY	Country of Register
BbgExhid	Char	Char String	12		Bloomberg Exchange ID
BbgExhtk	Char	Char String 40			Bloomberg Exchange Ticker
ExchgCD	Char	Char String	6	EXCHG	EDI maintained Exchange code.  Equivalent to the MIC code but necessary as MIC might not be available in a timely fashion.
MIC	Char	Char String	4	MICCODE	SWIFT MIC code
SecurityDesc	VarChar	Char String	70		
CurenCD	Char	Char String	3	CUREN	The applicable currency that the bond is denominated in
SectyCD	Char	Char String	3	SECTYPE	Identifies Fixed Income Instruments
IssuerName	VarChar	Char String	70		
GICS	Char	Char VarChar	8		General Industry Classification Standard.  GICS is developed by MSCI and S&P
CntryofIncorp	Char	Char String	2	CNTRY	
Isstype	Char	Char String	10	ISSTYPE	whether GOVERNMENT, CORPORATE, AGENCY etc.
InterestRate	Numeric	v14.5	14		Interest Rate applicable to the debt security
MatPrice	Numeric	v19.6	20		Maturity Price
NominalValue	Numeric	v14.5	14		
OutstandingAmount	Numeric	v18.5	18		Outstanding Amount in millions
ParValue	Numeric	v14.5	14		"Indicates the nominal values that the debt security is issued in - eg USD1000, USD10000"
DenominationMultiple	BigINT	64 bit		Trading denomination multiples		
MaturityDate	Date	yyyy/mm/dd	10		The date on which the security redeems. The security is then cancelled.
MaturityStructure	Char	Char String	1	DEBTSTATUS	Identifies if the bond is Convertible, Redeemable or Exchangeable, details of which are captured separately	
IndusID	Integer	32 bit	10	INDUS	
CFI	Char	Char String	10		ISO CFI - Classification of Financial Instruments. Alphabetical code consisting of 6 letters. 
CIK	Char	Char VarChar	10		Central Index Key Code
SIC	Char	Char VarChar	10		Standard Industrial Classification code
Tier	TinyInt	Integer 1	3		Tier levels of capital requirement
UppLow	Char	Char String	1	UPPLOW	Sub division of tier structure
SeniorJunior	VarChar	Char String	1	SENJUN 	Indicates either Senior or Junior debt. Senior has first claim on collateral to be used for interest and principal payments and Junior second claim.
Subordinate	Char	Char String	1	YNBLANK	Indicates whether a security has Subordinated Debt. These pay interest and principal to investors secondary after all full payment to Senior Debt.
Guaranteed	Char	Char String	1	BOOLEAN	Indicates if a security has the benefit of a Guarantee. This is usually either a Parent Company or Government
Relationship	VarChar	Char String	10	RELATION	Agency Type
RegistrarName	VarChar	Char String	70		
StatusFlag	Char	Char String	1	STATUSFLAG	Inactive at the global level else security is active. Not to be confused with delisted which is inactive at the exchange level



----------					
Series ENSO WFI - LOOKUP - Combined Lookup Table -					

Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description
Actflag	Char	Char String	1	ACTION	Record Level Action Status
Changed	Date	yyyy/mm/dd	10		Last Changed date of record at EDI
*TypeGroup	Char	Char String	10	TYPEGROUP	Link between coded data and Combined Lookup Table
*Code	Char	Char String	10		Code data value
Lookup	VarChar	Char String	70		Lookup value for Code (previous field)

